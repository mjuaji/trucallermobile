package com.ubuntucellular.user;

import com.google.gson.Gson;
import com.ubuntucellular.util.Path;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

import java.io.IOException;
import java.util.HashMap;

public class UserController {
    private static OkHttpClient client = new OkHttpClient();
    private static Logger logger = LoggerFactory.getLogger(UserController.class);
    UserController(){}

    public static Object handleAuthCallback(Request req, Response res){
        return authCallback(req,res);
    }
    private static Object authCallback(Request req, Response res){
        res.type(Path.Web.JSON_TYPE);
        User authReceived = new Gson().fromJson(req.body(),User.class);
        logger.info("Received Auth: {}",authReceived);

        //fetch user using OkHttp3
        okhttp3.Request request = new okhttp3.Request.Builder()
                .header("Authorization","Bearer "+authReceived.getAccessToken())
                .url(authReceived.getEndpoint())
                .build();

        client.newCall(request).enqueue( new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final okhttp3.Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    // do something wih the result
                    logger.info("Auth Response: {}",response.body());
                }
            }
        });

        HashMap<String,Object> response = new HashMap<>();
        response.put("code",201);
        return new Gson().toJson(response);
    }

    public static Object handleLogin(Request request,Response response){
        return login(request,response);
    }
    private static Object login(Request request,Response response){
        response.type(Path.Web.JSON_TYPE);
        response.status(200);
        logger.info("Incoming Login Data:{}",request.body());

        HashMap<String,Object> returnRes = new HashMap<>();
        returnRes.put("code",201);
        return new Gson().toJson(returnRes);
    }
}
